# Rumble in the Box (Coding Challenge API)

## API
Die API findet sich im Verzeichnis `./api`. Alle weiteren Befehle werden aus diesem Verzeichnis heraus aufgerufen.

### Voraussetzungen

- [NodeJS >= v18.7.0](https://nodejs.org) um den Server-Code ausführen zu können
- [Docker Desktop](https://www.docker.com/products/docker-desktop/) (optional) um API Dokumentation anzuzeigen
- [Postman Client](https://www.postman.com/downloads/) (optional) um API zu testen

### Installation
```bash
$> npm i
```
### Start
```bash
$> npm start
```
### Test
```bash
$> npm test
```
### API Dokumentation
Die API wurde im OpenAPI 3 Standard dokumentiert. Dabei wird die API in einer Textdatei im `YAML` Format repräsentiert. Die Dokumentation kann über Tools wie Swagger "schöner" angezeigt werden. 
#### Browser
Dokumentation kann im [Swagger Hub](https://app.swaggerhub.com/apis/amlang/rumble-in_the_box/1.0.0) (SwaggeHub [ohne Quelle](https://app.swaggerhub.com/apis-docs/amlang/rumble-in_the_box/1.0.0)) oder unter [https://editor-next.swagger.io/](https://editor-next.swagger.io/) geöffnet und angezeigt werden.

#### Lokal
Lokal kann ein Docker Container ausgeführt werden. Docker Image laden und ausführen. Dabei den Root-Ordner des Projekts (im Beispiel `$PWD`) als `/foo`-Mountpoint einbinden. Port ist beliebig wählbar, im Beispiel wird `18180` verwendet.

Docker Image laden:
```bash
$> docker pull swaggerapi/swagger-ui
```
Docker Container starten:
```bash
$> docker run -it --rm -p 18180:8080 -e SWAGGER_JSON=/foo/api/open-api.yml -v ${PWD}:/foo swaggerapi/swagger-ui
```
Webseite im Browser öffnen:
```bash
$> open http://localhost:18180
```

### Postman Tests

Nach dem Import können die Routen in Postman einzeln oder als komplette Collection geöffnet und ausgeführt werden.

Postman Collection Testrunner öffnen
![Postman Collection Testrunner öffnen](./images/postman-collection-testrunner.png)
Postman Testrunner starten
![Postman Testrunner starten](./images/postman-testrunner-starten.png)
Postman Ergebnisse
![Postman Ergebnisse](./images/postman-ergebnisse.png)


## UI
### Installation
nicht benötigt
### Start
Datei `ui/index.html` in Browser (bevorzugt ein aktueller Chrome-Browser) öffnen.
Impressionen der UI finden sich im Verzeichnis `images`.

### Bekannter Fehler
Wird eine Schulung bearbeitet, verliert diese aktuell die Schulungstermine und wird nach dem Bearbeiten nicht richtig angezeigt.
Schulungstermine müssten "nur" in das Formular aufgenommen werden. Danach sollte der Fehler erledigt sein.
Bei dem Reload-Fehler hätte ich besser auf ein bewährtes UI Framework setzen sollen. 


## Verwendete Tools
- [Swagger](https://swagger.io/): API Dokumentation
- [Postman](https://www.postman.com/): API Test
- [Google Chrome](https://www.google.com/intl/de_de/chrome/)
- [VS Code](https://code.visualstudio.com/download)