export default [
    {
        "guid": "10e8547e0e",
        "title": "NodeJS RESTful API",
        "description": "This 24-hour course focuses on developing RESTful APIs with NodeJS for at least four hours. The remaining 20 hours are bridged with Lord of the Rings movies and PowerPoint presentations.",
        "teacher": "Jean Luc Picard",
        "price": 99.75,
        "dates": [
            {
                "date": "2023-07-02",
                "openSlots": 8
            },
            {
                "date": "2023-07-03",
                "openSlots": 7
            },
            {
                "date": "2023-07-05",
                "openSlots": 5
            },
            {
                "date": "2023-07-07",
                "openSlots": 3
            }
        ]
    },
    {
        "guid": "dac84655",
        "title": "Walking with the Deads",
        "description": "Daryl and Dog show you how to dance with the devil or walk with the dead in this 177-part training. Food and drinks are the participants themselves.",
        "teacher": "Daryl Dixon",
        "price": 167.33,
        "dates": [
            {
                "date": "2023-07-20",
                "openSlots": 8
            }
        ]
    },
    {
        "guid": "73e840e0",
        "title": "Full Warp experience",
        "description": "Give me warp in the factor of five, six, seven, eight!",
        "teacher": "William '#1' Riker",
        "price": 56.78,
        "dates": [
            {
                "date": "2023-07-11",
                "openSlots": 9
            },
            {
                "date": "2023-07-13",
                "openSlots": 7
            },
            {
                "date": "2023-07-17",
                "openSlots": 3
            },
            {
                "date": "2023-07-19",
                "openSlots": 1
            }
        ]
    },
    {
        "guid": "a541a6fc",
        "title": "How to become a personal deamon (Advanced)",
        "description": "Learn in this extended part the chaotic practices to become a personal demon of your own bean ",
        "teacher": "Luci (@Luci's Inferno)",
        "price": 666.00,
        "dates": [
            {
                "date": "2023-07-20",
                "openSlots": 0
            },
            {
                "date": "2023-07-23",
                "openSlots": 7
            },
            {
                "date": "2023-07-29",
                "openSlots": 1
            },
            {
                "date": "2023-07-31",
                "openSlots": 9
            }
        ]
    },
];