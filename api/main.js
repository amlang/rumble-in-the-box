import { config } from "./config.js";
import { server } from "./server/server.js";
import { trainings } from "./ressources/trainings.js";

server.register(trainings.router);
export default server.app;

if (process.env.NODE_ENV != 'test') {
    server.start(config.PORT);
}
