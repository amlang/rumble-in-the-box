
import express from 'express';
import database from '../db/database.js';
import moment from 'moment/moment.js';
import crypto from 'crypto';
import {APIError404} from '../server/api-error-404.js';
const router = express.Router();
const path = '/trainings';
/**
 * Overview of all trainings offered.
 * (optional) Offered trainings can be filtered by the query parameters `start` and `end`
 * 
 * @todo validation of the query parameters
 */
router.get(path, (req, res) => {
    let trainings = database;
    if(req.query.start && req.query.start !== '' && req.query.end && req.query.end !== ''){
        /**
         * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find?retiredLocale=de
         * The find() method returns the first element ...
         * 
         * https://momentjs.com/docs/#/query/is-between/
         * [...] The match is exclusive.
         */
        trainings = database.filter(e => e.dates?.some(d => moment(d.date).isBetween(req.query.start, req.query.end)));
    }
    if(trainings.length == 0){
        res.status(204).end();
    } else {
        res.send(trainings);
    }
});

/**
 * Add a new training.
 * Training dates are optional and can be added later.
 * Trainings without dates are not bookable.
 * 
 * @todo schema validation of the req.body
 * @todo authorization
 */
router.post(path, (req, res) => {

    const item = {
        guid: crypto.randomBytes(4).toString('hex'),
        ...req.body
    };
    database.push(item);
    res.set('Location', `/training/${item.guid}`);
    res.status(201).end();
});

/**
 * Display a training with posible training dates and available slots, price and teacher 
 * 
 * @todo validation of the path parameters
 */
router.get(`${path}/:trainingGuid`, (req, res) => {
    const training = database.find(e => e.guid === req.params.trainingGuid);
    if(training == null) {
        throw new APIError404();
    }
    res.send(training);
});

/**
 * Update a training
 * 
 * @todo validation of the req.body
 * @todo authorization
 */
router.put(`${path}/:trainingGuid`, (req, res) => {
    const index = database.findIndex(e => e.guid === req.params.trainingGuid);
    if(index == -1) {
        throw new APIError404();
    }
    database[index] = req.body;
    res.status(204).end();
});

/**
 * Delete a training
 * 
 * @todo validation of the path parameters
 * @todo authorization
 */
router.delete(`${path}/:trainingGuid`, (req, res) => {
    const index = database.findIndex(e => e.guid === req.params.trainingGuid);
    if(index == -1) {
        throw new APIError404();
    }
    database.splice(index, 1);
    res.status(200).end();
});

export const trainings = {
    router
}
