import server from '../main';
import supertest from 'supertest';
import assert from 'assert';

describe('Trainings Endpoints', () => {

    it('GET /trainings should show all trainings', (done) => {
      supertest(server).get('/api/v1/trainings')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            assert(response.body.length, 4);
            return done();
        });
    });
    it('GET /trainings/:trainingsGuid should show 404 not found', (done) => {
      supertest(server).get('/api/v1/trainings/randomstring')
        .expect(404, done);
    });
    it('GET /trainings/:trainingsGuid should show one trainings', (done) => {
      supertest(server).get('/api/v1/trainings/10e8547e0e')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            assert(response.body.guid, '10e8547e0e');
            return done();
        });
    });
    it('PUT /trainings/:trainingsGuid should show 404 not found', (done) => {
      supertest(server).put('/api/v1/trainings/randomstring')
        .expect(404, done);
    });
    const request = supertest(server);
    it('PUT /trainings/:trainingsGuid should modify teacher', async () => {
        let response = await request.put('/api/v1/trainings/10e8547e0e')
            .send({
                "guid": "10e8547e0e",
                "title": "NodeJS RESTful API",
                "description": "This 24-hour course focuses on developing RESTful APIs with NodeJS for at least four hours. The remaining 20 hours are bridged with Lord of the Rings movies and PowerPoint presentations.",
                "teacher": "Lt. Data",
                "price": 99.75,
            });
        expect(response.status).toEqual(204);
        response = await request.get('/api/v1/trainings/10e8547e0e')
        expect(response.status).toEqual(200);
        expect(response.body.teacher).toEqual('Lt. Data');
    });
    it('DELETE /trainings/:trainingsGuid should show 404 not found', (done) => {
      supertest(server).delete('/api/v1/trainings/randomstring')
        .expect(404, done);
    });
    it('DELETE /trainings/:trainingsGuid should delete item', async () => {
        let response = await request.delete('/api/v1/trainings/10e8547e0e');
        expect(response.status).toEqual(200);
        response = await request.get('/api/v1/trainings/10e8547e0e')
        expect(response.status).toEqual(404);
    });
  });