import { APIError } from "./api-error.js";

export class APIError404 extends APIError {
    constructor(instance = null){
        super();
        this.type =  '/errors/ressource-not-found';
        this.title =  'Ressource not found';
        this.status =  404;
        this.detail =  'Requested ressource could not be found';
        this.instance =  instance;
        this.message = `404 - ${this.title}`;
    }
}