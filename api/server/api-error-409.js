import { APIError } from "./api-error.js";

export class APIError409 extends APIError {
    constructor(detail, instance = null){
        super();
        this.type =  '/errors/validation-error';
        this.title =  'Validation error';
        this.status =  409;
        this.detail =  detail;
        this.instance =  instance;
        this.message = `409 - ${this.title}`;
    }
}