import { APIError } from "./api-error.js";

export class APIError500 extends APIError {
    constructor(detail){
        super();
        this.type =  '/errors/server-error';
        this.title =  'Unhandled Server Error';
        this.status =  500;
        this.detail =  detail;
        this.instance =  '';
    }
    static fromError(error) {
        const err = new APIError500(error.stack);
        err.title = error.message;
        err.message = `500 - ${error.message}`;
        err.stack = error.stack;
        return err;
    }
}