import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import cors from 'cors';

import { APIError } from './api-error.js';
import { APIError404 } from './api-error-404.js';
import { APIError500 } from './api-error-500.js';

const logger = morgan;
const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(helmet());
app.use(cors());

export const server = {
    start: (port) => {
        /**
         * Intercept undefined routes and throw them as 404 Not found with our APIError404 as error.
         */
        app.get('*', (req, _res) => {
            throw new APIError404(req.path);
        });
        /**
         * Exception handler
         */
        app.use((err, req, res, _next) => {
            /**
             * Any unwanted error should be enclosed here as HTTP 500 with our APIError500 class
             */
            if(!(err instanceof APIError)){
                err = APIError500.fromError(err);
            }
            // If the error did not specify a path, add here
            err.instance = err.instance || req.path;
            console.error(err, err.stack);
            res.status(err.status).send(err);
        });
        /**
         * @todo graceful shutdown
         */
        app.listen(port, () => console.log(`Example app listening on port ${port}`))
    },
    register: (router) => app.use('/api/v1', router),
    app
}

